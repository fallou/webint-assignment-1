
function onload(){
  if (sessionStorage.getItem("user")){
    document.getElementById("connexion").style.display = "none";
    document.getElementById("deconnexion").style.display = "block";
    document.getElementById("addComment").style.display = "block";
  }
  var n = localStorage.length
  if(n >0){
    for(i = 0; i<n ; i++){
      console.log(i);
      writeComment(localStorage.getItem("comment" + i),i)
    }
  }
  
}




function changeVideo(){
    var source = document.getElementById("recherche").value;
    var video = document.getElementById('video');

    video.src = (source); 
    video.setAttribute('autoplay', '');
}

function goTo(){
    var video = document.getElementById('video');
    var time = document.getElementById('time').value;

    if (time < video.duration){video.currentTime = time;}
    else alert ("the offset is too high")
    
    
}

function test(){
  navigator.geolocation.getCurrentPosition(pos =>console.log(pos));
  var loc = AutocompleteService();
  var result = loc.getPlacePredictions(new google.maps.LatLng(-34, 151));
  console.log(result)
}

function rotate() {
  var video = document.getElementById("video");
  
  const current = video.style.transform
  console.log(typeof(current))
  if (current == 'rotate(90deg)'){
      document.getElementById("video").style.transform = 'rotate(180deg)';
      console.log('1');
      document.getElementById("espace").style.display = "none";
      document.getElementById("espace1").style.display = "none";
      
  }
  if (current == 'rotate(180deg)'){
      document.getElementById("video").style.transform = 'rotate(270deg)';
      console.log('2');
      document.getElementById("espace").style.display = "block";
      document.getElementById("espace1").style.display = "block";
  }
  if (current == 'rotate(270deg)'){
      document.getElementById("video").style.transform = 'rotate(0deg)';
      console.log('3')
      document.getElementById("espace").style.display = "none";
      document.getElementById("espace1").style.display = "none";
  }
  if((current == 'rotate(0deg)') || (current=="")){
      document.getElementById("video").style.transform = 'rotate(90deg)';
      document.getElementById("espace").style.display = "block";
      document.getElementById("espace1").style.display = "block";
      console.log('4')
  }
}

function preview(){
    var preview = document.getElementById("videoPreview")
    preview.setAttribute("src", document.getElementById("video").src + "#t=25")
    preview.style.display = "block"
}


function controls() {
  var checkbox = document.querySelector("input[name=checkbox]");
  var video = document.getElementById('video');
  if (checkbox.checked) {
    video.setAttribute("controls", "true")
  } else {
    video.removeAttribute("controls");
  }
}


function submit(){
  var firstName = document.getElementById("firstName").value
  var lastName =document.getElementById("lastName").value;
  var email = document.getElementById("email").value
  var tel = document.getElementById("tel").value

  if(firstName != "" && lastName != "" && email != "" && tel !=""){
    var user = []
    user.push(firstName);
    user.push(lastName);
    user.push(email);
    user.push(tel);
    sessionStorage.setItem("user", JSON.stringify(user));
    document.getElementById("connexion").style.display = "none";
    document.getElementById("deconnexion").style.display = "block";
    document.getElementById("addComment").style.display = "block";
  }
  else alert("Please fill in the required fields.");
}

function logOut(){
  sessionStorage.removeItem("user");
    document.getElementById("connexion").style.display = "block";
    document.getElementById("deconnexion").style.display = "none";
    document.getElementById("addComment").style.display = "none";
}

function comments() {
  var comments = document.getElementById("commentaires");

}

function addComment() {
  var newComment = document.getElementById("newComment").value
  if (newComment) {
    var months = ["January ", "February ", "March ", "April ", "May ", "June ", "July ", "August ", "September ", "October ", "November ", "December "]
    var user = JSON.parse(sessionStorage.getItem("user"));
    var name = user[0] + " " + user [1];
    var d = JSON.stringify(Date());
    var date = d.substring(4, 16);
    var location = null; 
    var commentInfo = JSON.stringify([newComment, name, date, location]);
    var n = localStorage.length;
    //alert(JSON.stringify(commentInfo))
    localStorage.setItem("comment" + n, commentInfo )

    writeComment(localStorage.getItem("comment" + n), n)
  }
  
}

function writeComment(commentInfo, n) {
  var cInfo = JSON.parse(commentInfo);
  var comments = document.getElementById("commentaires");
  var comment = (document.getElementById("usualComment").cloneNode());
  comment.id = "comment" + n;
  comment.style.display = "block";
  comments.appendChild(comment);
  var pseudo = (document.getElementById("usualPseudo").cloneNode());
  var comm = (document.getElementById("usualComm").cloneNode());
  var date = (document.getElementById("usualDate").cloneNode());
  pseudo.id = "pseudo" + n;
  comm.id = "comm" + n;
  date.id = "date" + n;
  pseudo.innerHTML = cInfo[1];
  comm.innerHTML = cInfo[0];
  date.innerHTML = cInfo[2];
  comment.appendChild(pseudo);
  comment.appendChild(comm);
  comment.appendChild(date);


  //comment.setAttribute("class", "commentaire");
  //var pseudo = comment.appendChild("div");
  //pseudo.setAttribute("class", "pseudo");
  //pseudo.value= cInfo[1];
  //console.log(comments);
  //console.log(cInfo);
  //console.log(comment)
}

//1156px